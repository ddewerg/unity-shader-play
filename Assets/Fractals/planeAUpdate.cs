﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class planeAUpdate : MonoBehaviour {

    Renderer r;

    // Use this for initialization
    void Start () {
        r = GetComponent<Renderer>();
	}
	
	// Update is called once per frame
	void Update () {
        transform.position = new Vector3(0, 5 * Mathf.Sin(Time.time), 0);
        r.material.SetVector("_C", new Vector4(-transform.position.x / 5, -transform.position.z / 5, transform.position.y/5, valSetter.val));
	}
}
