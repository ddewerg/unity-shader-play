﻿Shader "Unlit/Julia1"
{
	Properties
	{
		_MainTex ("Texture", 2D) = "white" {}
		_C ("C", Vector) = (0,0,0,0)
		_Iterations("Iterations", Int) = 200
	}
	SubShader
	{
		Tags { "RenderType"="Opaque" }
		LOD 100
		Cull Off
		Pass
		{
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			
			#include "UnityCG.cginc"

			struct appdata
			{
				float4 vertex : POSITION;
				float2 uv : TEXCOORD0;
			};

			struct v2f
			{
				float2 uv : TEXCOORD0;
				float4 vertex : SV_POSITION;
			};

			sampler2D _MainTex;
			float4 _C;
			int _Iterations;

			v2f vert (appdata v)
			{
				v2f o;
				o.vertex = UnityObjectToClipPos(v.vertex);
				o.uv = v.uv;
				return o;
			}
			
			float2 squareComplex(float2 complex)
			{
				return float2(complex.x * complex.x - complex.y * complex.y, 2 * complex.x * complex.y);
			}

			fixed4 frag (v2f i) : SV_Target
			{
				int j = 0;
				float2 tuv = ((i.uv * 2) - 1);
				float2 z = float2(0, tuv.x) + _C.xy;
				for (j = 0; j < _Iterations; j++)
				{
					z = squareComplex(z) + float2(tuv.y, 0) + _C.zw;
					if (length(z) > 2)
						break;
				}
				// sample the texture
				fixed4 col = tex2D(_MainTex, float2(j/(float)_Iterations, 0));
				return col;
			}
			ENDCG
		}
	}
}
