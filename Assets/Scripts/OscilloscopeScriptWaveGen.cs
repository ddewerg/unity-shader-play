﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum Equation
{
    Sin,
    Cos,
    Square,
    Saw
}


[System.Serializable]
public class WaveComponent
{
    public Equation equ;
    public float frequency;
    public float phaseOffset;
    [Range(0,1)]
    public float volume;
}


public class OscilloscopeScriptWaveGen : MonoBehaviour {
    AudioSource AS;
    Renderer r;

    public List<WaveComponent> leftWaveform;
    public List<WaveComponent> rightWaveform;

    int sampleOffset = 0;

    float[] sampleL = new float[1023];
    float[] sampleR = new float[1023];
    float[] wdata = new float[2048];

    float GenWave(int sample, WaveComponent waveForm)
    {
        switch (waveForm.equ)
        {
            case Equation.Sin:
                return waveForm.volume * Mathf.Sin((waveForm.frequency/44100 * (sample + sampleOffset)) + waveForm.phaseOffset);
            case Equation.Cos:
                return waveForm.volume* Mathf.Cos((waveForm.frequency/44100 * (sample + sampleOffset)) + waveForm.phaseOffset);
            case Equation.Square:
                return waveForm.volume * Mathf.Sign(Mathf.Sin((waveForm.frequency / 44100 * (sample + sampleOffset)) + waveForm.phaseOffset));
            case Equation.Saw:
                return waveForm.volume * (((waveForm.frequency / 44100 * (sample + sampleOffset)) - Mathf.Floor((waveForm.frequency / 44100 * (sample + sampleOffset)))));
            default:
                return 0;
        }
    }

    void OnAudioFilterRead(float[] data, int channels)
    {
        for(int i = 0; i < data.Length/2; i++)
        {
            wdata[i * 2] = data[i*2] = 0;
            wdata[(i * 2) + 1] = data[(i * 2) + 1] = 0;
            foreach (var item in leftWaveform)
            {
                wdata[i*2] = data[i * 2] += GenWave(i, item);                
            }
            foreach (var item in rightWaveform)
            {
                wdata[(i*2) + 1] = data[(i * 2) + 1] = GenWave(i, item);

            }
        }
        sampleOffset += data.Length;
    }

	// Use this for initialization
	void Start () {
        AS = GetComponent<AudioSource>();
        AS.clip = AudioClip.Create("CustomWaveForm", 1023, 2, 44100, false);
        AS.Play();
	}
	
	// Update is called once per frame
	void Update () {
        for(int i = 0; i < sampleL.Length; i++)
        {
            sampleL[i] = wdata[i*2];
            sampleR[i] = wdata[(i * 2) + 1];
        }
        Shader.SetGlobalFloatArray("sampleL", sampleL);
        Shader.SetGlobalFloatArray("sampleR", sampleR);
    }
}
