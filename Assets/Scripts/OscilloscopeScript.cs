﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OscilloscopeScript : MonoBehaviour {
    AudioSource AS;
    Renderer r;
	// Use this for initialization
	void Start () {
        AS = GetComponent<AudioSource>();
        
	}
	
	// Update is called once per frame
	void Update () {
        float playbackPos = AS.time/AS.clip.length;
        int samplePos = Mathf.RoundToInt(AS.clip.samples * playbackPos);
        float[] samples = new float[1023 * AS.clip.channels];
        AS.clip.GetData(samples, samplePos);
        if (AS.clip.channels == 2)
        {
            float[] sampleL = new float[1023];
            float[] sampleR = new float[1023];
            for(int i = 0; i < 1023; i++)
            {
                sampleL[i] = samples[i * 2];
                sampleR[i] = samples[(i * 2) + 1];

            }
            Shader.SetGlobalFloatArray("sampleL", sampleL);
            Shader.SetGlobalFloatArray("sampleR", sampleR);

        }
        else
        {
            float[] sampleR = new float[1023];
            for(int i = 0; i < 1023; i++)
            {
                sampleR[i] = (1.0f /1024) *i;
            }
            Shader.SetGlobalFloatArray("sampleL", samples);
            Shader.SetGlobalFloatArray("sampleR", sampleR);
        }
	}
}
