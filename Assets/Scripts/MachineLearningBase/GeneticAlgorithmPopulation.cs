﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
//using SFB;

namespace MachineLearning
{

    public class GeneticAlgorithmPopulation : MonoBehaviour
    {
        [SerializeField]
        GeneticAlgorithmGenome genomePrefab;
        [SerializeField]
        int popPerMachine;
        [SerializeField]
        int genomeLength;
        [SerializeField]
        AnimationCurve rouletteProbability;
        [SerializeField]
        Vector2[] timePerGeneration;
        [SerializeField]
        float minFitnessThreshold = 0;

        [SerializeField]
        int generation = 0;
        int timeIndex = 0;
        float timeGenStart;
        string maxFileName, avgFileName, creatureFileName;

        List<GeneticAlgorithmGenome> genomes;

        // Use this for initialization
        void Start()
        {
            genomes = new List<GeneticAlgorithmGenome>(popPerMachine);
            for (int i = 0; i < popPerMachine; i++)
            {
                genomes.Add(Instantiate<GeneticAlgorithmGenome>(genomePrefab));
                genomes[i].genome = new float[genomeLength];
                for (int j = 0; j < genomeLength; j++)
                {
                    genomes[i].genome[j] = Random.value;
                }
                genomes[i].Init();
                genomes[i].UpdateGenome();
            }
            timeGenStart = Time.time;
            maxFileName = "TestLogs/" + Random.Range(10, 100000).ToString();
            avgFileName = maxFileName + "_avg.csv";
            creatureFileName = maxFileName + "_creature.csv";
            maxFileName = maxFileName + "_max.csv";
            System.IO.File.WriteAllText(maxFileName, "");
            System.IO.File.WriteAllText(avgFileName, "");
            System.IO.File.WriteAllText(creatureFileName, "");
        }

        private void ResetGens()
        {
            for (int i = 0; i < popPerMachine; i++)
            {
                genomes[i].genome = new float[genomeLength];
                genomes[i].fitness = 0;
                for (int j = 0; j < genomeLength; j++)
                {
                    genomes[i].genome[j] = Random.value;
                }
                genomes[i].UpdateGenome();
            }
            timeGenStart = Time.time;
            maxFileName = maxFileName.Substring(0, maxFileName.Length - 4) + "1.csv";
            avgFileName = avgFileName.Substring(0, avgFileName.Length - 4) + "1.csv";
            creatureFileName = creatureFileName.Substring(0, creatureFileName.Length - 4) + "1.csv";
            System.IO.File.WriteAllText(maxFileName, "");
            System.IO.File.WriteAllText(avgFileName, "");
            System.IO.File.WriteAllText(creatureFileName, "");
            generation = 0;
            timeIndex = 0;
        }

        private void Update()
        {
            Time.timeScale = 1;
            if (Time.time > timeGenStart + timePerGeneration[timeIndex].x)
            {
                Time.timeScale = 0;
                Evolve();
                timeGenStart = Time.time;
            }
        }

        public void Evolve()
        {

            foreach (GeneticAlgorithmGenome g in genomes)
            {
                g.calculateFitness();
            }

            genomes.Sort();
            if (genomes[genomes.Count - 1].fitness < minFitnessThreshold)
            {
                ResetGens();
                return;
            }

            List<float[]> tGenomes = new List<float[]>(popPerMachine);
            float avg = 0;
            for (int i = 0; i < popPerMachine; i++)
            {
                avg += genomes[i].fitness;
                int ind1 = (int)(rouletteProbability.Evaluate(Random.value) * (popPerMachine - 1));
                int ind2 = (int)(rouletteProbability.Evaluate(Random.value) * (popPerMachine - 1));
                //int sliceIndex = (int)(genomeLength * 0.8f * Random.value + genomeLength * 0.1f);
                tGenomes.Add(new float[genomeLength]);
                //for (int j = 0; j < sliceIndex; j++)
                //{

                //    if (Random.value < 0.001)
                //        tGenomes[i][j] = Random.value;
                //    else
                //        tGenomes[i][j] = genomes[ind1].genome[j];
                //}
                //for(int j = sliceIndex; j < genomeLength; j++)
                //{
                //    if (Random.value < 0.001)
                //        tGenomes[i][j] = Random.value;
                //    else
                //        tGenomes[i][j] = genomes[ind2].genome[j];
                //}
                if (i < popPerMachine * 0.2)
                {
                    for (int j = 0; j < genomeLength; j++)
                    {
                        tGenomes[i][j] = Random.value;
                    }
                }
                else
                {
                    for (int j = 0; j < genomeLength; j++)
                    {
                        if (Random.value < 0.001)
                        {
                            tGenomes[i][j] = Random.value;

                        }
                        else
                        {
                            tGenomes[i][j] = (Random.value < 0.5) ? genomes[ind1].genome[j] : genomes[ind2].genome[j];
                        }
                    }
                }
            }

            avg /= popPerMachine;

            print("Max Fitness: " + genomes[popPerMachine - 1].fitness.ToString() + ", Average Fitness: " + avg.ToString());
            System.IO.File.AppendAllText(maxFileName, genomes[popPerMachine - 1].fitness.ToString() + ",");
            System.IO.File.AppendAllText(avgFileName, avg.ToString() + ",");
            if (generation > 1 && generation % 25 == 0)
            {
                for (int i = 0; i < genomeLength; i++)
                {
                    System.IO.File.AppendAllText(creatureFileName, genomes[popPerMachine - 1].genome[i] + ",");
                }
                System.IO.File.AppendAllText(creatureFileName, "\n");
            }
            for (int i = 0; i < popPerMachine; i++)
            {
                genomes[i].genome = (float[])tGenomes[i].Clone();
                genomes[i].fitness = 0;
                genomes[i].UpdateGenome();
            }
            generation++;
            if (generation > timePerGeneration[timeIndex].y)
                timeIndex = Mathf.Min(timeIndex + 1, timePerGeneration.Length - 1);
        }



    }
}