﻿using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;

namespace MachineLearning
{
    public class GeneticAlgorithmGenome : MonoBehaviour, IComparable
    {

        public float fitness = 0;
        public float[] genome;

        public int CompareTo(object obj)
        {
            if (fitness < ((GeneticAlgorithmGenome)obj).fitness)
            {
                return -1;
            }
            else if (fitness == ((GeneticAlgorithmGenome)obj).fitness)
            {
                return 0;
            }
            return 1;
        }

        public virtual void Init()
        {

        }

        public virtual void UpdateGenome()
        {
            SendMessage("UpdateFromGenome", genome, SendMessageOptions.RequireReceiver);
        }

        public virtual void calculateFitness()
        {

        }
    }
}