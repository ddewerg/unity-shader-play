﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace MachineLearning
{

    public class RecurrentNet : MonoBehaviour
    {

        public int delay = 3;
        public int brainDim = 16;

        [System.NonSerialized]
        public Queue<float[]> brainBuffers;
        [System.NonSerialized]
        public float[] weights;
        [System.NonSerialized]
        public int[] connectivity;
        public float[] curBuf;

        // Use this for initialization
        public void Init()
        {
            brainBuffers = new Queue<float[]>();
            for (int i = 0; i < delay; i++)
            {
                float[] b = new float[brainDim];
                brainBuffers.Enqueue(b);
            }
            weights = new float[brainDim * 4];
            connectivity = new int[brainDim * 4];
        }
        // Update is called once per frame
        public void NetUpdate(float[] input)
        {
            curBuf = brainBuffers.Dequeue();
            //BrainProcessing
            float[] inBuf = brainBuffers.Peek();
            for (int i = 0; i < input.Length; i++)
            {
                inBuf[i] = input[i];
            }
            for (int i = 0; i < brainDim; i++)
            {
                curBuf[i] = inBuf[connectivity[i * 4]] * weights[i * 4];
                curBuf[i] += inBuf[connectivity[i * 4 + 1]] * weights[i * 4 + 1];
                curBuf[i] += inBuf[connectivity[i * 4 + 2]] * weights[i * 4 + 2];
                curBuf[i] += inBuf[connectivity[i * 4 + 3]] * weights[i * 4 + 3];
                curBuf[i] = -1 + (2 / (1 + Mathf.Exp(-curBuf[i])));
            }
            brainBuffers.Enqueue(curBuf);
        }

        public void resetBrain()
        {
            brainBuffers.Clear();
            for (int i = 0; i < delay; i++)
            {
                float[] b = new float[brainDim];
                brainBuffers.Enqueue(b);
            }
        }

        void UpdateFromGenome(float[] r)
        {
            for (int i = 0; i < brainDim * 4; i++)
            {
                connectivity[i] = Mathf.RoundToInt(r[i] * (brainDim - 1));
                weights[i] = (r[i + (brainDim * 4)] * 2) - 1;
            }
        }
    }

}