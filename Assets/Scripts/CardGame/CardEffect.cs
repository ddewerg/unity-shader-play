﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum CardEffectActivators
{
    ETB,
    LeaveBattlefield,
    Upkeep,
    Draw,
    Damage,
    SpellPlayed
}

public abstract class CardEffect : ScriptableObject {

    Card target;
    public CardEffectActivators activator;

    public abstract void Activate();

    public abstract void PollTarget();
}
