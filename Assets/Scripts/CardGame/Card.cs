﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum CardType
{
    Permanent,
    Transient
}

public enum CardSpeed
{
    Instant,
    Slow
}

public enum CardAlignment
{
    Light,
    Dark,
    Arcane,
    Bio
}

public class Card : ScriptableObject {
    public CardType type;
    public CardSpeed speed;

    public CardEffect[] effects;

    public int manaCost;

    public void Cast()
    {

    }
}
