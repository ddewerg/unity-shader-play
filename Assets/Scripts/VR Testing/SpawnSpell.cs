﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Valve.VR.InteractionSystem;
public class SpawnSpell : MonoBehaviour {

    public SteamVR_TrackedObject device;

    public SpringSpell spellPrefab;
    SpringSpell instance;

    public void summonSpell()
    {
        instance = Instantiate<SpringSpell>(spellPrefab);
        instance.transform.position = transform.position;
        attachSpell(instance);
    }

    public void attachSpell(SpringSpell inst)
    {
        instance = inst;
        SpringJoint s = instance.gameObject.AddComponent<SpringJoint>();
        s.connectedBody = GetComponent<Rigidbody>();
        s.anchor = Vector3.zero;
        s.autoConfigureConnectedAnchor = false;
        s.connectedAnchor = Vector3.zero;
        s.spring = 100;
        s.damper = 0.2f;
        instance.GetComponent<Rigidbody>().drag = 1.0f;

    }

    private void Update()
    {
        if(SteamVR_Controller.Input((int)device.index).GetPressDown(SteamVR_Controller.ButtonMask.Trigger))
        {
            summonSpell();
        }
        if (SteamVR_Controller.Input((int)device.index).GetPressUp(SteamVR_Controller.ButtonMask.Trigger))
        {
            releaseSpell();
        }

        if (Input.GetMouseButtonDown(0))
        {
            summonSpell();
        }
        if(Input.GetMouseButtonUp(0))
        {
            releaseSpell();
        }
    }

    public void releaseSpell()
    {
        instance.SendMessage("DestroyJoint");
        instance.GetComponent<Rigidbody>().drag = 0;
    }
}
