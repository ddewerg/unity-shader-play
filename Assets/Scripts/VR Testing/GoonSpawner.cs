﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GoonSpawner : MonoBehaviour {

    public Damageable goonPrefab;
    public int maxGoons;
    List<Damageable> goons;

	// Use this for initialization
	void Start () {
        goons = new List<Damageable>();
        for (int i = 0; i < maxGoons; i++)
        {
            SpawnGoon();
        }
	}
	
    void SpawnGoon()
    {
        Damageable goon = (Instantiate<Damageable>(goonPrefab));
        goon.transform.position = Random.insideUnitSphere * 50;
        goons.Add(goon);
    }

	// Update is called once per frame
	void Update () {
		if(goons.Count < maxGoons)
        {
            SpawnGoon();
        }

        foreach(Damageable g in goons)
        {
            if(g.health == 0)
            {
                DespawnGoon(g);
                break;
            }
        }

        
	}

    public void DespawnGoon(Damageable dgoon)
    {
        goons.Remove(dgoon);
        Destroy(dgoon.gameObject);
    }
}
