﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Damageable : MonoBehaviour {

    public float health
    {
        get
        {
            return _health;
        }

        set
        {
            _health = Mathf.Max(0, value);
        }
    }

    private float _health;
    public float initialHealth = 100;


    void TakeDamage(float amount)
    {
        health -= amount;
    }


	// Use this for initialization
	void Start () {
        _health = initialHealth;
	}
}
