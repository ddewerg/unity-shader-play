﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Damaging : MonoBehaviour {

    public float damage = 50;

    private void OnTriggerEnter(Collider other)
    {
        other.SendMessage("TakeDamage", damage);
    }
}
