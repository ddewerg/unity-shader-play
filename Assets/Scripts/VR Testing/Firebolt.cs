﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Firebolt : MonoBehaviour {

    public GameObject spawnEffect;

    private void OnTriggerEnter(Collider other)
    {
        Instantiate<GameObject>(spawnEffect).transform.position = this.transform.position;
        Destroy(this);
    }
}
