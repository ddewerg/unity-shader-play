﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum SpellClass
{
    Bolt,
    Beam
}

public struct ChildDataStruct
{
    public Vector3 direction;
    public Vector3 projectileVelocity;
}


public class SpringSpell : MonoBehaviour {

    public SpellClass spellClass;
    public GameObject childObject;
    public GameObject parent;

    void SpawnChild()
    {
        GameObject g = Instantiate<GameObject>(childObject);
        g.transform.position = transform.position;
        ChildDataStruct cd = new ChildDataStruct();
        cd.direction = parent.transform.position;
        cd.projectileVelocity = GetComponent<Rigidbody>().velocity;
        g.SendMessage("OnSpawn", cd);
        
    }


    void DestroyJoint()
    {
        switch (spellClass)
        {
            case SpellClass.Beam:
                SpawnChild();
                Destroy(gameObject);
                break;
            case SpellClass.Bolt:
                Destroy(GetComponent<SpringJoint>());
                break;
        }

    }
}
