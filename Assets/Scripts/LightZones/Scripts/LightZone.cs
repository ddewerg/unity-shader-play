﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LightZone : MonoBehaviour {

    public AnimationCurve lightIntensityCurve;
    public float lifeTime = 60;
    public GameObject lightZoneObject;
    public float zoneRadii = 10;
    public float edgeHardness = 0;
    Vector4[] currentZones = new Vector4[10];
    float[] times = new float[10];
    bool[] active = new bool[10];

    private void Start()
    {
        for(int i = 0; i < 10; i++)
        {
            times[i] = 0;
            active[i] = false;
            currentZones[i] = new Vector4();
        }
    }

    public bool SpawnZone(Vector3 location)
    {
        for(int i = 0; i < 10; i++)
        {
            if(!active[i])
            {
                active[i] = true;
                times[i] = 0;
                currentZones[i] = new Vector4(location.x, location.y, location.z, 0);
                if (lightZoneObject != false)
                {
                    GameObject g = Instantiate<GameObject>(lightZoneObject);
                    g.transform.position = location;
                    StartCoroutine(DestroyObj(g));
                }
                return true;
            }
        }
        return false;
    }

	
	// Update is called once per frame
	void Update () {
        for(int i = 0; i < 10; i++)
        {
            if (active[i])
            {
                times[i] += Time.deltaTime;
                active[i] = times[i] < lifeTime;
                currentZones[i].w = active[i] ? lightIntensityCurve.Evaluate(times[i]/lifeTime) : 0;
            }
        }
        Shader.SetGlobalFloat("ZoneRadii", zoneRadii);
        Shader.SetGlobalFloat("EdgeHardness", edgeHardness);
        Shader.SetGlobalVectorArray("GlowZones", currentZones);
	}

    private void OnDrawGizmos()
    {
        for(int i = 0; i < 10; i++)
        {
            if(active[i])
            {
                Gizmos.color = new Color(times[i] / lifeTime, 0, 0, 1);
                Gizmos.DrawWireSphere(new Vector3(currentZones[i].x, currentZones[i].y, currentZones[i].z), zoneRadii);
            }
        }
    }

    IEnumerator DestroyObj(Object obj)
    {
        yield return new WaitForSeconds(lifeTime);
        Destroy(obj);
    }
}
