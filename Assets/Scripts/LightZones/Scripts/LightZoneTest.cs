﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(LightZone))]
public class LightZoneTest : MonoBehaviour {

    LightZone lz;

    public float cooldown = 10;
    bool onCooldown = false;

	// Use this for initialization
	void Start () {
        lz = GetComponent<LightZone>();
	}
	
	// Update is called once per frame
	void Update () {
	    if(Input.GetButtonDown("Fire1") && !onCooldown)
        {
            print(lz.SpawnZone(transform.position));
            onCooldown = true;
            StartCoroutine(CDReset());
        }
	}

    IEnumerator CDReset()
    {
        yield return new WaitForSeconds(cooldown);
        onCooldown = false;
    }
}
