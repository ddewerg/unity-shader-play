﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[RequireComponent(typeof(MeshRenderer))]
public class RenderVolumeTest : MonoBehaviour {

    MeshRenderer mr;
    int eyePosID;
    RenderTexture rt;
    [SerializeField]
    ComputeShader computeShader;
    int frame = 0;
    // Use this for initialization
    void Start () {
        mr = GetComponent<MeshRenderer>();

        rt = new RenderTexture(128, 128, 0);
        rt.dimension = UnityEngine.Rendering.TextureDimension.Tex3D;
        rt.volumeDepth = 128;
        rt.enableRandomWrite = true;
        rt.wrapMode = TextureWrapMode.Clamp;
        rt.Create();
	}
	
	// Update is called once per frame
	void Update () {
        int id = computeShader.FindKernel("CSMain");
        computeShader.SetInt("texW", rt.width);
        computeShader.SetInt("texH", rt.height);
        computeShader.SetInt("texD", rt.depth);
        computeShader.SetInt("frame", frame++);
        computeShader.SetTexture(id, "Result", rt);
        computeShader.Dispatch(id, rt.width / 8, rt.height / 8, rt.volumeDepth / 8);
        mr.material.SetTexture("_MainTex", rt);
    }
}
