﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FluidSim : MonoBehaviour {

    MeshRenderer mr;
    int eyePosID;
    public RenderTexture rt, vt;
    [SerializeField]
    ComputeShader computeShader;
    //int frame = 0;
    // Use this for initialization
    void Start()
    {
        mr = GetComponent<MeshRenderer>();

        rt = new RenderTexture(128, 128, 0);
        rt.dimension = UnityEngine.Rendering.TextureDimension.Tex3D;
        rt.volumeDepth = 128;
        rt.enableRandomWrite = true;
        rt.wrapMode = TextureWrapMode.Clamp;
        rt.Create();

        vt = new RenderTexture(128, 128, 0);
        vt.dimension = UnityEngine.Rendering.TextureDimension.Tex3D;
        vt.volumeDepth = 128;
        vt.enableRandomWrite = true;
        vt.wrapMode = TextureWrapMode.Clamp;
        vt.Create();

        int id = computeShader.FindKernel("Init");
        computeShader.SetInt("texW", rt.width);
        computeShader.SetInt("texH", rt.height);
        computeShader.SetInt("texD", rt.depth);
        computeShader.SetTexture(id, "Result", rt);
        computeShader.Dispatch(id, rt.width / 8, rt.height / 8, rt.volumeDepth / 8);
    }

    // Update is called once per frame
    void Update()
    {
        int id = computeShader.FindKernel("CSMain");
        computeShader.SetInt("texW", rt.width);
        computeShader.SetInt("texH", rt.height);
        computeShader.SetInt("texD", rt.depth);
        computeShader.SetFloat("deltaTime", Time.deltaTime);
        computeShader.SetTexture(id, "Result", rt);
        computeShader.SetTexture(id, "Input", vt);
        computeShader.Dispatch(id, rt.width / 8, rt.height / 8, rt.volumeDepth / 8);
        computeShader.SetTexture(id, "Result", vt);
        computeShader.SetTexture(id, "Input", rt);
        computeShader.Dispatch(id, rt.width / 8, rt.height / 8, rt.volumeDepth / 8);
        mr.material.SetTexture("_MainTex", rt);
    }

}
