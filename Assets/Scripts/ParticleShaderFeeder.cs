﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ParticleShaderFeeder : MonoBehaviour {
    ParticleSystem ps;
    ParticleSystem.Particle[] particles = new ParticleSystem.Particle[1023];
    Vector4[] pPos = new Vector4[1023];
    // Use this for initialization
	void Start () {
        ps = GetComponent<ParticleSystem>();
	}
	
	// Update is called once per frame
	void Update () {
		for(int i = 0;  i < 1023; i++)
        {
            pPos[i] = Vector4.zero;
        }

        ps.GetParticles(particles);
        for(int i = 0; i < Mathf.Min(1023, ps.particleCount); i++)
        {
            Vector3 p = transform.TransformPoint(particles[i].position);
            pPos[i] = new Vector4(p.x, p.y, p.z, 1);
        }
        Shader.SetGlobalVectorArray("DeformPoints", pPos);
	}
}
