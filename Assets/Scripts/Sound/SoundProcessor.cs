﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundProcessor : MonoBehaviour {

    public AudioSource sound;
    float[] freqData;

    void Start()
    {
        freqData = new float[512];
    }
	// Update is called once per frame
	void Update () {
        Shader.SetGlobalFloatArray("PrevSound", freqData);
        sound.GetSpectrumData(freqData, 0, FFTWindow.BlackmanHarris);
        Shader.SetGlobalFloatArray("Sound",freqData);
	}
}
