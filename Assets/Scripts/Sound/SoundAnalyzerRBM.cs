﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MathNet.Numerics.LinearAlgebra;
using System.Threading;


public class SoundAnalyzerRBM : MonoBehaviour {
    Matrix<float> Weights;
    public Matrix<float> Input;
    public Matrix<float> Output;

    public int numTrainingRuns = 5;
    public AudioSource sound;


    float[] wveData = new float[512];

    bool startFlag = false;

	// Use this for initialization
	void Start () {
        Input = CreateMatrix.Dense<float>(1, 512);
        Output = CreateMatrix.Dense<float>(1, 20);
        Weights = CreateMatrix.Random<float>(512, 20);
        StartCoroutine(LearnSong());
	}
	
    IEnumerator LearnSong()
    {
        for(int i = 0; i < numTrainingRuns; i++)
        {
            for(int j = 0; j < sound.clip.length * 60; j++)
            {
                float[] waveData = new float[512];
                sound.time = j/60.0f;
                sound.GetSpectrumData(waveData, 0, FFTWindow.BlackmanHarris);
                int k = 0;
                Input.MapInplace((x) => waveData[k++], Zeros.Include);
                Matrix<float> compMat = Input.Clone();
                Matrix<float> err = getContrastiveDivergenceError(compMat, 5);
                Output = (compMat * Weights).Map((x) => 1 / (1 + Mathf.Exp(x)), Zeros.Include);
                Matrix<float> deriv = err.Transpose() * Output;
                Weights -= deriv * 0.01f;
                if (j % 60 == 0)
                {
                    print("1 second of audio processed");
                    yield return 0;
                }
            }
        }
        startFlag = true;
    }


    Matrix<float> getContrastiveDivergenceError(Matrix<float> compMat, int iterations)
    {
        for(int i = 0; i < iterations; i++)
        {
            Output = (Input * Weights).Map((x) => 1/(1+Mathf.Exp(x)), Zeros.Include);
            Input = (Output * Weights.Transpose()).Map((x) => 1 / (1 + Mathf.Exp(x)), Zeros.Include);
        }

        return Input - compMat;
    }

    void Update()
    {
        if(startFlag)
        {
            sound.time = 0;
            sound.Play();
            startFlag = false;
        }
        if(sound.isPlaying)
        {
            sound.GetSpectrumData(wveData, 0, FFTWindow.BlackmanHarris);
            int k = 0;
            Input.MapInplace((x) => wveData[k++], Zeros.Include);
            Output = (Input * Weights).Map((x) => 1 / (1 + Mathf.Exp(x)), Zeros.Include);
            Shader.SetGlobalFloatArray("ProcSound", Output.AsColumnMajorArray());

        }

    }
}
