﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "DoNothingAction", menuName = "AIActions/DoNothing")]
public class AIDoNothing : AIAction
{
    public override float calculateUrgency(AIAgent agent, GameObject target)
    {
        //return urgency = <value> returns the value as well as setting urgency to it 
        return urgency = 0;
    }

    public override void doAction(AIAgent agent, GameObject target)
    {
        agent.stats.hungry += 0.01f;

    }
}
