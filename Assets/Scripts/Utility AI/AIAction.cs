﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AIAction : ScriptableObject, IComparable {
    public float urgency = 0;

    public int CompareTo(object obj)
    {
        if (urgency < ((AIAction)obj).urgency)
        {
            return -1;
        }
        else if (urgency == ((AIAction)obj).urgency)
        {
            return 0;
        }
        return 1;
    }

    public virtual float calculateUrgency(AIAgent agent, GameObject target)
    {
        //return urgency = <value> returns the value as well as setting urgency to it 
        return urgency = 0;
    }

    public virtual void doAction(AIAgent agent, GameObject target)
    {
        //Fill in logic here in derived classes

    }
}
