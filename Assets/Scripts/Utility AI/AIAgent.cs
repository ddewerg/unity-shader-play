﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[System.Serializable]
public struct StatBlock
{
    public float hungry;
}


public class AIAgent : MonoBehaviour {
    public StatBlock stats;
	// Use this for initialization
	void Start () {
        stats.hungry = 0;
	}
}
