﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AIManager : MonoBehaviour {
    public List<AIAgent> Agents;
    public List<AIAction> Actions;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		foreach(AIAgent a in Agents)
        {
            foreach(AIAction act in Actions)
            {
                //i didnt put in a way to set the target, ill leave you to figure that one out yourself
                act.calculateUrgency(a, null);
            }
            Actions.Sort();
            //see above about the null
            Actions[Actions.Count-1].doAction(a, null);
        }

        //might also want a way to make this not happen every frame, again, that one is up to you
	}
}
