﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "RequestHealingAction", menuName = "AIActions/RequestHealing")]
public class AIRequestHealing : AIAction
{
    public override float calculateUrgency(AIAgent agent, GameObject target)
    {
        return urgency = agent.stats.hungry;
    }

    public override void doAction(AIAgent agent, GameObject target)
    {
        Debug.Log(agent.name + " needs healing");
        agent.stats.hungry = -5;
    }
}
