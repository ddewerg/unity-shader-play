﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MachineLearning;

[System.Serializable]
public struct Bodypart
{
    public HingeJoint hinge;
    public Vector3 offset;
    public Quaternion baseRot;
}


[RequireComponent(typeof(RecurrentNet))]
public class RecurrentWalker : GeneticAlgorithmGenome
{
    public GameObject core;
    public Material coreMat;
    public List<Bodypart> bodyParts;
    RecurrentNet rn;
    public Vector3 baseOffset;
    public Quaternion baseRot;
    public float springForce = 5;
    public float springDamp = 0;
    private float legActivity = 0;
    private float[] prevBuf;
    float time = 0;
    public override void Init()
    {
        rn = GetComponent<RecurrentNet>();
        rn.Init();
        prevBuf = new float[rn.brainDim];
        coreMat = core.GetComponent<Renderer>().material;

    }

    void Update()
    {
        time += Time.deltaTime;
        List<float> inp = new List<float>(bodyParts.Count + 4);
        inp.Add(transform.position.x);
        inp.Add(transform.position.y);
        inp.Add(transform.position.z);
        inp.Add(time);
        for(int i = 0; i < bodyParts.Count; i++)
        {
            inp.Add(bodyParts[i].hinge.angle);
        }

        rn.NetUpdate(inp.ToArray());
        for(int i = 0; i < bodyParts.Count; i++)
        {
            JointSpring j = new JointSpring();
            j.damper = springDamp;
            j.spring = springForce;
            j.targetPosition =  (bodyParts[i].hinge.spring.targetPosition + (rn.curBuf[(rn.brainDim - 1) - i] * (rn.curBuf[(rn.brainDim - 1) - i] < 0 ? -bodyParts[i].hinge.limits.min : bodyParts[i].hinge.limits.max)))/2;
            bodyParts[i].hinge.spring = j;
            legActivity += Mathf.Abs(rn.curBuf[(rn.brainDim - 1) - i] - prevBuf[(rn.brainDim - 1) - i]/3); //
            prevBuf = rn.curBuf;
        }
        Vector3 vel = core.GetComponent<Rigidbody>().velocity;
        legActivity *= Time.deltaTime;
        fitness += -Mathf.Abs(vel.z) + legActivity + vel.x + Mathf.Abs(vel.y/5) - ((Mathf.Abs(core.transform.position.y - baseOffset.y) > 0.7) ? Mathf.Abs(core.transform.position.y - baseOffset.y) : 0);
        if (vel.magnitude <= 0.1)
            fitness -= 10;
        //fitness += legActivity * 5;
        coreMat.color = new Color(fitness / 400, fitness < 200 ? fitness / 200 : 1 - (fitness-200)/400, fitness / -100);
        legActivity = 0;
    }

    public override void UpdateGenome()
    {
        core.transform.localPosition = baseOffset;
        core.transform.localRotation = baseRot;
        springDamp = 0.2f + 30 * genome[genome.Length - 1];
        springForce = 1 + genome[genome.Length - 2] * 99;
        for (int i = 0; i < bodyParts.Count; i++)
        {
            bodyParts[i].hinge.transform.localPosition = bodyParts[i].offset;
            bodyParts[i].hinge.transform.localRotation = bodyParts[i].baseRot;
        }
        legActivity = 0;
        time = 0;
        base.UpdateGenome();
    }

    public override void calculateFitness()
    {
    }
}
