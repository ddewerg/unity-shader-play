﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MachineLearning;
public class CreatureLoader : MonoBehaviour {

    public RecurrentWalker walkerPrefab;
    public int brainNo = 0;
    public int genomeLen = 1026;

    public RecurrentWalker walkerInstance;
	// Use this for initialization
	void Start () {
        string filename = "FILE";
        float[] genome = new float[genomeLen];
        walkerInstance = Instantiate<RecurrentWalker>(walkerPrefab);
        System.IO.StreamReader sr = new System.IO.StreamReader(filename);
        for(int i = 0; i < brainNo; i++)
        {
            sr.ReadLine();
        }
        string brLin = sr.ReadLine();
        if(brLin == null)
        {
            sr.Close();
            return;
        }

        string[] brStr = brLin.Split(',');
        for(int i = 0; i < Mathf.Min(brStr.Length, genome.Length); i++)
        {
            genome[i] = float.Parse(brStr[i]);
        }
        walkerInstance.genome = genome;
        walkerInstance.Init();
        walkerInstance.UpdateGenome();
        sr.Close();
        
	}
}
