﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ButtonScript : MonoBehaviour {

    public GameObject buttonObject;
    public Softcube cubeToSpawn;
    public Vector3 spawnLoc;
    public bool pressed = false;

    void OnTriggerEnter(Collider other)
    {
        if (!pressed)
        {
            Softcube s = Instantiate<Softcube>(cubeToSpawn);
            s.transform.position = spawnLoc;
            s.c = new Color(1, 0, 0);
            buttonObject.transform.localScale = new Vector3(1, 0.25f, 1);
            pressed = true;
        }
    }
}
