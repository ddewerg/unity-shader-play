﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PhysGrabber : MonoBehaviour {

    Rigidbody grabbedBody;
    float distance;

	// Update is called once per frame
	void Update () {
		if(Input.GetButtonDown("Fire1"))
        {
            RaycastHit rc = new RaycastHit();
            if (Physics.Raycast(transform.position, transform.forward, out rc, 10000))
            {
                grabbedBody = rc.rigidbody;
                if(grabbedBody)
                    grabbedBody.isKinematic = true;
                distance = rc.distance;
            }
        }
        if (Input.GetButtonUp("Fire1"))
        {
            if(grabbedBody)
                grabbedBody.isKinematic = false;
            grabbedBody = null;
        }
        if (grabbedBody != null)
        {
            grabbedBody.MovePosition(transform.position + transform.forward * distance);
        }
	}
}
