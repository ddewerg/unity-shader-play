﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Softcube : MonoBehaviour {

    public uint resolution = 10;
    public uint mass = 1;
    public Rigidbody vertex;
    public float springiness = 100;
    public float damping = 0.3f;
    public Material m;
    public Color c;
    Rigidbody[] shell;
    int[] ind;
    List<Vector3> verts;
    MeshFilter mf;
    MeshRenderer mr;
    Rigidbody[,,] vertices;
    List<Vector3> norms = new List<Vector3>();
    // Use this for initialization
    void Start()
    {
        mf = gameObject.AddComponent<MeshFilter>();
        mr = gameObject.AddComponent<MeshRenderer>();
        mr.material = m;
        mr.material.color = c;
        vertices = new Rigidbody[resolution, resolution, resolution];
        shell = new Rigidbody[6 * resolution * resolution];
        verts = new List<Vector3>();
        ind = new int[72 * (resolution - 1) * (resolution - 1)];

        for (int x = 0; x < resolution; x++)
        {
            for (int y = 0; y < resolution; y++)
            {
                for (int z = 0; z < resolution; z++)
                {
                    Rigidbody r = Instantiate<Rigidbody>(vertex);
                    r.transform.position = transform.position;
                    r.transform.SetParent(transform);
                    r.transform.localPosition = (new Vector3(x - resolution / 2, y - resolution / 2, z - resolution / 2) / resolution);
                    r.mass = mass / (resolution * resolution * resolution);
                    vertices[x, y, z] = r;
                    if(x == 0)
                    {
                        shell[y + resolution * z] = r;
                        verts.Add(r.transform.localPosition);
                        norms.Add(new Vector3(0, 0, 0));
                    }
                    if (y == 0)
                    {
                        shell[(resolution*resolution) +  x + resolution * z] = r;
                        verts.Add(r.transform.localPosition);
                        norms.Add(new Vector3(0, 0, 0));
                    }
                    if(z == 0)
                    {
                        shell[(2 * resolution * resolution) + x + resolution * y] = r;
                        verts.Add(r.transform.localPosition);
                        norms.Add(new Vector3(0, 0, 0));
                    }
                    if (x == resolution - 1)
                    {
                        shell[(3 * resolution * resolution) + y + resolution * z] = r;
                        verts.Add(r.transform.localPosition);
                        norms.Add(new Vector3(0, 0, 0));
                    }
                    if (y == resolution - 1)
                    {
                        shell[(4 * resolution * resolution) + x + resolution * z] = r;
                        verts.Add(r.transform.localPosition);
                        norms.Add(new Vector3(0, 0, 0));
                    }
                    if (z == resolution - 1)
                    {
                        shell[(5 * resolution * resolution) + x + resolution * y] = r;
                        verts.Add(r.transform.localPosition);
                        norms.Add(new Vector3(0, 0, 0));
                    }
                }
            }
        }


        for(int i = 0; i < 6; i++)
        {
            int sideOffset = (int)(i * resolution * resolution);
            int indexOffset = (int)(i * (resolution - 1) * (resolution - 1) * 12);
            int l = 0;
            for (int j = 0; j < resolution - 1; j++)
            {
                for (int k = 0; k < resolution - 1; k++)
                {
                    ind[l + (indexOffset)] = sideOffset + j + k * (int)resolution; l++;
                    ind[l + (indexOffset)] = sideOffset + (j+1) + k * (int)resolution; l++;
                    ind[l + (indexOffset)] = sideOffset + j + (k+1) * (int)resolution; l++;
                    ind[l + (indexOffset)] = sideOffset + j + (k + 1) * (int)resolution; l++;
                    ind[l + (indexOffset)] = sideOffset + (j + 1) + k * (int)resolution; l++;
                    ind[l + (indexOffset)] = sideOffset + (j + 1) + (k + 1) * (int)resolution; l++;


                    ind[l + (indexOffset)] = sideOffset + j + k * (int)resolution; l++;
                    ind[l + (indexOffset)] = sideOffset + j + (k + 1) * (int)resolution; l++;
                    ind[l + (indexOffset)] = sideOffset + (j + 1) + k * (int)resolution; l++;
                    ind[l + (indexOffset)] = sideOffset + (j + 1) + k * (int)resolution; l++;
                    ind[l + (indexOffset)] = sideOffset + j + (k + 1) * (int)resolution; l++;
                    ind[l + (indexOffset)] = sideOffset + (j + 1) + (k + 1) * (int)resolution; l++;
                }
            }
        }

        mf.mesh.SetVertices(verts);
        mf.mesh.SetTriangles(ind, 0);
 

        for (int x = 0; x < resolution; x++)
        {
            for (int y = 0; y < resolution; y++)
            {
                for (int z = 0; z < resolution; z++)
                {
                    SpringJoint s;
                    if (z != 0)
                    {
                        s = vertices[x, y, z].gameObject.AddComponent<SpringJoint>();
                        s.damper = damping;
                        s.spring = springiness;
                        s.connectedBody = vertices[x, y, z - 1];
                    }
                    if (y != 0)
                    {
                        s = vertices[x, y, z].gameObject.AddComponent<SpringJoint>();
                        s.damper = damping;
                        s.spring = springiness;
                        s.connectedBody = vertices[x, y - 1, z];
                    }
                    if (x != 0)
                    {
                        s = vertices[x, y, z].gameObject.AddComponent<SpringJoint>();
                        s.damper = damping;
                        s.spring = springiness;
                        s.connectedBody = vertices[x - 1, y, z];
                    }


                    if (x != 0 && z != 0)
                    {
                        s = vertices[x, y, z].gameObject.AddComponent<SpringJoint>();
                        s.damper = damping;
                        s.spring = springiness;
                        s.connectedBody = vertices[x - 1, y, z - 1];
                    }
                    if (x != 0 && y != 0)
                    {
                        s = vertices[x, y, z].gameObject.AddComponent<SpringJoint>();
                        s.damper = damping;
                        s.spring = springiness;
                        s.connectedBody = vertices[x - 1, y - 1, z];
                    }

                    if (y != 0 && z != 0)
                    {
                        s = vertices[x, y, z].gameObject.AddComponent<SpringJoint>();
                        s.damper = damping;
                        s.spring = springiness;
                        s.connectedBody = vertices[x, y - 1, z - 1];
                    }


                    if (x != 0 && y != 0 && z != 0)
                    {
                        s = vertices[x, y, z].gameObject.AddComponent<SpringJoint>();
                        s.damper = damping;
                        s.spring = springiness;
                        s.connectedBody = vertices[x - 1, y - 1, z - 1];
                    }


                    if (x < resolution - 2 && z != 0)
                    {
                        s = vertices[x, y, z].gameObject.AddComponent<SpringJoint>();
                        s.damper = damping;
                        s.spring = springiness;
                        s.connectedBody = vertices[x + 1, y, z - 1];
                    }

                    if (x < resolution - 2 && y != 0)
                    {
                        s = vertices[x, y, z].gameObject.AddComponent<SpringJoint>();
                        s.damper = damping;
                        s.spring = springiness;
                        s.connectedBody = vertices[x + 1, y - 1, z];
                    }
                    if (y != 0 && z < resolution - 2)
                    {
                        s = vertices[x, y, z].gameObject.AddComponent<SpringJoint>();
                        s.damper = damping;
                        s.spring = springiness;
                        s.connectedBody = vertices[x, y - 1, z + 1];
                    }

                    if (x != 0 && y != 0 && z < resolution - 2)
                    {
                        s = vertices[x, y, z].gameObject.AddComponent<SpringJoint>();
                        s.damper = damping;
                        s.spring = springiness;
                        s.connectedBody = vertices[x - 1, y - 1, z + 1];
                    }

                    if (x < resolution - 2 && y != 0 && z < resolution - 2)
                    {
                        s = vertices[x, y, z].gameObject.AddComponent<SpringJoint>();
                        s.damper = damping;
                        s.spring = springiness;
                        s.connectedBody = vertices[x + 1, y - 1, z + 1];
                    }

                    if (x < resolution - 2 && y != 0 && z != 0)
                    {
                        s = vertices[x, y, z].gameObject.AddComponent<SpringJoint>();
                        s.damper = damping;
                        s.spring = springiness;
                        s.connectedBody = vertices[x + 1, y - 1, z - 1];
                    }
                }
            }
        }

    }
	
	// Update is called once per frame
	void Update () {
        Vector3 avgVec = Vector3.zero;
		for(int i = 0; i < shell.Length; i++)
        {
            avgVec += verts[i] = shell[i].transform.localPosition;
        }
        avgVec /= shell.Length;

        mf.mesh.SetVertices(verts);
        mf.mesh.RecalculateBounds();
        mf.mesh.RecalculateNormals();
        for (int i = 0; i < verts.Count; i++)
        {
            norms[i] = (verts[i] - avgVec).normalized;
        }
        mf.mesh.SetNormals(norms);

    }
}
