﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Cloth : MonoBehaviour
{

    public uint resolution = 10;
    public Rigidbody vertex;
    public float springiness;
    public float damping;
    // Use this for initialization
    void Start()
    {
        Rigidbody[,] vertices = new Rigidbody[resolution, resolution];
        for (int x = 0; x < resolution; x++)
        {
            for (int y = 0; y < resolution; y++)
            {
                Rigidbody r = Instantiate<Rigidbody>(vertex);
                r.transform.position = transform.position;
                r.transform.SetParent(transform);
                r.transform.localPosition = (new Vector3(x - resolution / 2, 0, y - resolution / 2) / resolution);
                vertices[x, y] = r;
            }
        }

    }

    // Update is called once per frame
    void Update()
    {

    }
}
