﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

#if UNITY_EDITOR
[CustomEditor(typeof(RecurrentWalker))]
public class WalkerInspector : Editor
{
    public override void OnInspectorGUI()
    {
        DrawDefaultInspector();
        if (GUILayout.Button("Generate Offsets"))
        {
            ((RecurrentWalker)target).baseOffset = ((RecurrentWalker)target).core.transform.localPosition;
            ((RecurrentWalker)target).baseRot = ((RecurrentWalker)target).core.transform.localRotation;

            for (int i = 0; i < ((RecurrentWalker)target).bodyParts.Count; i++)
            {
                Bodypart b = new Bodypart();
                b.hinge = ((RecurrentWalker)target).bodyParts[i].hinge;
                b.offset = b.hinge.gameObject.transform.localPosition;
                b.baseRot = b.hinge.gameObject.transform.localRotation;
                ((RecurrentWalker)target).bodyParts[i] = b;
            }
        }
    }
        
}
#endif