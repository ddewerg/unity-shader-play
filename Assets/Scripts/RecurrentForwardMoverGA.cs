﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MachineLearning;

[RequireComponent(typeof(RecurrentNet))]
public class RecurrentForwardMoverGA : GeneticAlgorithmGenome {

    RecurrentNet rn;

    public override void Init()
    {
        rn = GetComponent<RecurrentNet>();
        rn.Init();
    }

    void Update()
    {
        float[] inp = { transform.position.x, transform.position.y, transform.position.z };
        rn.NetUpdate(inp);
        transform.position = transform.position + new Vector3(rn.curBuf[rn.brainDim-1], rn.curBuf[rn.brainDim-2], 0) * Time.deltaTime * 5;
    }

    public override void UpdateGenome()
    {
        transform.position = new Vector3(0, 0.5f, 0);
        base.UpdateGenome();
    }

    public override void calculateFitness()
    {
        fitness = transform.position.x - Mathf.Abs(transform.position.y * 4);
    }
}
