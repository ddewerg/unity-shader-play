﻿using System.Collections;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace RevolutionGames.Saving
{
    [System.Serializable]
    public struct SceneGraph
    {
        public Dictionary<string, Dictionary<string, System.Object>> graph;
        public string curScene;
    }

    public class PersistenceManager : MonoBehaviour
    {
        SceneGraph savedData = new SceneGraph();
        bool first = false;

        void Awake()
        {
            if (!first)
            {
                if (FindObjectsOfType<PersistenceManager>().Length > 1)
                    Destroy(gameObject);
                savedData.graph = new Dictionary<string, Dictionary<string, System.Object>>();
                DontDestroyOnLoad(this);
                first = true;
            }
        }

        void OnEnable()
        {
            //Tell our 'OnLevelFinishedLoading' function to start listening for a scene change as soon as this script is enabled.
            SceneManager.sceneLoaded += OnLevelFinishedLoading;
        }

        void OnDisable()
        {
            //Tell our 'OnLevelFinishedLoading' function to stop listening for a scene change as soon as this script is disabled. 
            SceneManager.sceneLoaded -= OnLevelFinishedLoading;
        }


        /// <summary>
        /// Add each scene to the list or update saved entities in the list
        /// </summary>
        /// <param name="scene">The scene being loaded</param>
        /// <param name="mode">The mode of scene loading</param>
        void OnLevelFinishedLoading(Scene scene, LoadSceneMode mode)
        {
            print("loading scene");
            savedData.curScene = scene.name;
            if (savedData.graph.ContainsKey(scene.name))
            {
                //All instances of saved objects in the scene
                SavedBehavior[] savedInstances = FindObjectsOfType<SavedBehavior>();

                foreach (var item in savedInstances)
                {
                    item.Load(savedData.graph[scene.name]);
                }
            }
            else
            {
                savedData.graph.Add(scene.name, new Dictionary<string, System.Object>());

                SavedBehavior[] savedInstances = FindObjectsOfType<SavedBehavior>();

                foreach (var item in savedInstances)
                {
                    savedData.graph[scene.name].Add(item.name, item.Save());
                }
            }
        }


        /// <summary>
        /// Saves persistent data to file
        /// </summary>
        /// <param name="filename"> The name and extension of the saved file (path handled automatically)</param>
        public void SaveToFile(string filename)
        {
            Scene curScene = SceneManager.GetActiveScene();

            savedData.curScene = curScene.name;

            //Ensure all data from scene is current
            if (savedData.graph.ContainsKey(curScene.name))
            {
                //All instances of saved objects in the scene
                SavedBehavior[] savedInstances = GameObject.FindObjectsOfType<SavedBehavior>();

                foreach (var item in savedInstances)
                {
                    if (savedData.graph[curScene.name].ContainsKey(item.name))
                    {
                        savedData.graph[curScene.name][item.name] = item.Save();
                    }
                    else
                    {
                        savedData.graph[curScene.name].Add(item.name, item.Save());
                    }
                }
            }
            else
            {
                savedData.graph.Add(curScene.name, new Dictionary<string, System.Object>());

                SavedBehavior[] savedInstances = GameObject.FindObjectsOfType<SavedBehavior>();

                foreach (var item in savedInstances)
                {
                    savedData.graph[curScene.name].Add(item.name, item.Save());
                }
            }

            BinaryFormatter formatter = new BinaryFormatter();
            Stream saveStream = new FileStream(string.Concat(Application.persistentDataPath, filename), FileMode.Create, FileAccess.Write);
            formatter.Serialize(saveStream, savedData);
            saveStream.Close();
        }

        /// <summary>
        /// Loads persistent data from file
        /// </summary>
        /// <param name="filename">The name and extension of the saved file (path handled automatically)</param>
        public void LoadFromFile(string filename)
        {
            BinaryFormatter formatter = new BinaryFormatter();
            Stream loadStream = new FileStream(string.Concat(Application.persistentDataPath, filename), FileMode.Open, FileAccess.Read);
            savedData = (SceneGraph)formatter.Deserialize(loadStream);
            loadStream.Close();
            SceneManager.LoadScene(savedData.curScene);
        }

    }
}