﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace RevolitionGames.Saving
{
    public class SaveEventDispatcher
    {

        public delegate void SaveLoadAction(string path);
        public static event SaveLoadAction OnSave;
        public static event SaveLoadAction OnLoad;


        public static void SaveGame(string path)
        {
            OnSave(path);
        }

        public static void LoadGame(string path)
        {
            OnLoad(path);
        }
    }
}