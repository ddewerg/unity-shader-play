﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using RevolutionGames.Saving;

public class TestSavedObject : RevolutionGames.Saving.SavedBehavior {


    Renderer r;

    Color c = new Color(0,0,0);

    public override void Load(Dictionary<string, System.Object> p)
    {
        float[] v = (float[])p[name];
        c = new Color(v[0], v[1], v[2], v[3]);
    }

    public override System.Object Save()
    {
        float[] co = {c.r, c.g, c.b, c.a};
        return co;
    }

    // Use this for initialization
    void Start () {
        r = GetComponent<Renderer>();
	}
	
	// Update is called once per frame
	void Update () {
        r.material.color = c;

        if (Input.GetButtonDown("Fire1"))
            c = (c == Color.red) ? Color.black : Color.red;

        if (Input.GetButtonDown("Fire2"))
        {
            FindObjectOfType<PersistenceManager>().SaveToFile("Test");
            UnityEngine.SceneManagement.SceneManager.LoadScene("SaveTest2");
        }
	}
}
