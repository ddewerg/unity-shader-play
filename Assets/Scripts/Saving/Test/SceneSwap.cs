﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using RevolutionGames.Saving;

public class SceneSwap : MonoBehaviour {
	
	// Update is called once per frame
	void Update () {
        if (Input.GetButtonDown("Fire2"))
        {
            FindObjectOfType<PersistenceManager>().SaveToFile("Test");
            UnityEngine.SceneManagement.SceneManager.LoadScene("SaveTest1");
        }
    }
}
