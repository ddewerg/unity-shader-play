﻿// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

Shader "Custom/Crosshatch"
{
	Properties
	{
		_MainTex ("Texture", 2D) = "white" {}
	}
	SubShader
	{
		// No culling or depth
		Cull Off ZWrite Off ZTest Always

		Pass
		{
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			
			#include "UnityCG.cginc"

			struct appdata
			{
				float4 vertex : POSITION;
				float2 uv : TEXCOORD0;
			};

			struct v2f
			{
				float2 uv : TEXCOORD0;
				float4 vertex : SV_POSITION;
			};

			v2f vert (appdata v)
			{
				v2f o;
				o.vertex = UnityObjectToClipPos(v.vertex);
				o.uv = v.uv;
				return o;
			}
			
			sampler2D _MainTex;

			fixed4 frag (v2f i) : SV_Target
			{
				fixed4 col = tex2D(_MainTex, i.uv);
				float intensity = col.x + col.y + col.z;
				intensity *= col.w;
				intensity = round(intensity) + 1;
				int fhatch = (int)round((i.uv.x + i.uv.y + 1) * 50);
				//uint bhatch = (uint)((i.uv.x - i.uv.y + 1) * 500);
				if (fhatch % (int)intensity * 3 <= 1)
					col = col;
					//col = fixed4(0, 0, 0, 1);
				else
					col = fixed4(1, 1, 1, 1);
				return col;
			}
			ENDCG
		}
	}
}
