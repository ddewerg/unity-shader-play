﻿Shader "Unlit/Oscilloscope"
{
	Properties
	{
		_Color("Color", Color) = (0,1,0,1)
		_Gain("Gain", Float) = 1
		_LineWidth("LineWidth", Float) = 0.99
		_Brightness("Brightness", Float) = 10
	}
	SubShader
	{
		Tags { "RenderType"="Opaque" }
		LOD 100

		Pass
		{
			CGPROGRAM

			#pragma vertex vert
			#pragma fragment frag
			// make fog work
			#pragma multi_compile_fog
			
			#include "UnityCG.cginc"

			struct appdata
			{
				float4 vertex : POSITION;
				float2 uv : TEXCOORD0;
			};

			struct v2f
			{
				float2 uv : TEXCOORD0;
				float4 vertex : SV_POSITION;
			};

			fixed4 _Color;
			uniform float sampleL[1023];
			uniform float sampleR[1023];
			float _Gain;
			float _LineWidth;
			float _Brightness;

			v2f vert (appdata v)
			{
				v2f o;
				o.vertex = UnityObjectToClipPos(v.vertex);
				o.uv = v.uv;
				return o;
			}
			
			fixed4 frag (v2f i) : SV_Target
			{
				// sample the texture
				fixed4 col = fixed4(0,0,0,0);
				for (uint j = 0; j < 1023; j++)
				{
					float2 sCoord = float2((_Gain * sampleL[j] + 1)/2, (_Gain * sampleR[j] + 1)/2);
					float dist = length(i.uv - sCoord);
					col += _Brightness * step(dist, _LineWidth)/1024;
				}
				return _Color * col;
			}
			ENDCG
		}
	}
}
