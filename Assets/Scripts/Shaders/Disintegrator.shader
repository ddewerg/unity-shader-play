﻿Shader "Unlit/Disintegrator"
{
	Properties
	{
		_MainTex ("Texture", 2D) = "white" {}
		_DeformAmount("Deform", Float) = 0.5
		_DeformDistance("EffectorDistance", Float) = 0.5
		_DeformOffset("Offset", Float) = 0
		_Deform2D3D("Deform 3D Factor", Float) = 1
		_WireframeColor("Wireframe Color", Color) = (1,1,1,0)
	}
	SubShader
	{
		Tags { "RenderType"="Opaque" }
		LOD 100

		Pass
		{
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			#pragma geometry geom
			
			#include "UnityCG.cginc"
			#include "Common.cginc"

			struct appdata
			{
				float4 vertex : POSITION;
				float3 normal : NORMAL;
				float2 uv : TEXCOORD0;
				float distanceFromCentre : TEXCOORD1;
			};

			struct varyings
			{
				float4 pos : SV_POSITION;
				float3 normal : NORMAL;
				float2 uv : TEXCOORD0;
				float4 bary : TEXCOORD1;
			};

			sampler2D _MainTex;
			float4 _MainTex_ST;
			fixed4 _WireframeColor;

			void vert (inout appdata v)
			{
				v.distanceFromCentre = length(v.vertex);
				v.vertex = mul(unity_ObjectToWorld, v.vertex);
				v.normal = UnityObjectToWorldNormal(v.normal);
				
			}
			

			varyings VOut(float3 wpos, half3 wnrm, float2 uv, float4 bary)
			{
				varyings o;
				o.pos = UnityWorldToClipPos(float4(wpos, 1));
				o.normal = wnrm;
				o.uv = uv;
				o.bary = bary;
				return o;
			}

			float _DeformAmount;
			float _DeformOffset;
			float _DeformDistance;
			float4 DeformPoints[1023];
			float _Deform2D3D;

			[maxvertexcount(12)]
			void geom(triangle appdata input[3], uint pid : SV_PrimitiveID, inout TriangleStream<varyings> outStream)
			{
				uint seed = pid * 877;
				
				float3 n_0 = input[0].normal;
				float3 n_1 = input[1].normal;
				float3 n_2 = input[2].normal;


				float d0 = 0;
				float d1 = 0;
				float d2 = 0;
				
				for (int i = 0; i < 1023; i++)
				{
					d0 += (1 - clamp(length(DeformPoints[i].xyz - input[0].vertex) / _DeformDistance, 0, 1)) * DeformPoints[i].w;
					d1 += (1 - clamp(length(DeformPoints[i].xyz - input[1].vertex) / _DeformDistance, 0, 1)) * DeformPoints[i].w;
					d2 += (1 - clamp(length(DeformPoints[i].xyz - input[2].vertex) / _DeformDistance, 0, 1)) * DeformPoints[i].w;
				}

				float3 vOff0 = n_0 * (Random(seed++) * input[0].distanceFromCentre * _DeformAmount + _DeformOffset) * d0;
				float3 vOff1 = n_1 * (Random(seed++) * input[1].distanceFromCentre * _DeformAmount + _DeformOffset) * d1;
				float3 vOff2 = n_2 * (Random(seed++) * input[2].distanceFromCentre * _DeformAmount + _DeformOffset) * d2;

				float3 p_0 = input[0].vertex + vOff0;
				float3 p_1 = input[1].vertex + vOff1;
				float3 p_2 = input[2].vertex + vOff2;

				float3 scaleCentroid = (p_0 + p_1 + p_2) / 3;

				p_0 += (scaleCentroid - p_0) * d0;
				p_1 += (scaleCentroid - p_1) * d1;
				p_2 += (scaleCentroid - p_2) * d2;
				
				float d3_Unscaled = (d0 + d1 + d2) / 3;
				float d3 = _Deform2D3D * (d0 + d1 + d2) / 3;
				float3 n_3 = cross(normalize(p_0 - p_1), normalize(p_0 - p_2));
				float nOffScale = (length(p_0 - p_1) + length(p_0 - p_2) + length(p_1 - p_2)) / 3;
				float3 p_3 = ((p_0 + p_1 + p_2) / 3) + n_3 * nOffScale * _DeformAmount * d3;		


				if (_DeformAmount * d3 > 0)
				{
					outStream.Append(VOut(p_2, -n_2, input[0].uv, float4(1, 0, 0, 1 - d3_Unscaled)));
					outStream.Append(VOut(p_1, -n_1, input[1].uv, float4(0, 1, 0, 1 - d3_Unscaled)));
					outStream.Append(VOut(p_0, -n_0, input[2].uv, float4(0, 0, 1, 1 - d3_Unscaled)));
					outStream.RestartStrip();

					outStream.Append(VOut(p_2, float3(0, 0, 0), input[2].uv, float4(1, 0, 0, 1 - d3_Unscaled)));
					outStream.Append(VOut(p_3, float3(0, 0, 0), input[0].uv, float4(0, 1, 0, 1 - d3_Unscaled)));
					outStream.Append(VOut(p_1, float3(0, 0, 0), input[1].uv, float4(0, 0, 1, 1 - d3_Unscaled)));
					outStream.RestartStrip();

					outStream.Append(VOut(p_1, float3(0, 0, 0), input[1].uv, float4(1, 0, 0, 1 - d3_Unscaled)));
					outStream.Append(VOut(p_3, float3(0, 0, 0), input[2].uv, float4(0, 1, 0, 1 - d3_Unscaled)));
					outStream.Append(VOut(p_0, float3(0, 0, 0), input[0].uv, float4(0, 0, 1, 1 - d3_Unscaled)));
					outStream.RestartStrip();

					outStream.Append(VOut(p_2, float3(0, 0, 0), input[2].uv, float4(1, 0, 0, 1 - d3_Unscaled)));
					outStream.Append(VOut(p_0, float3(0, 0, 0), input[0].uv, float4(0, 1, 0, 1 - d3_Unscaled)));
					outStream.Append(VOut(p_3, float3(0, 0, 0), input[1].uv, float4(0, 0, 1, 1 - d3_Unscaled)));
					outStream.RestartStrip();
				}
				else
				{
					outStream.Append(VOut(p_0, n_2, input[0].uv, float4(1, 0, 0, 1 - d3_Unscaled)));
					outStream.Append(VOut(p_1, n_1, input[1].uv, float4(0, 1, 0, 1 - d3_Unscaled)));
					outStream.Append(VOut(p_2, n_0, input[2].uv, float4(0, 0, 1, 1 - d3_Unscaled)));
					outStream.RestartStrip();
				}
			}

			fixed4 frag (varyings i) : SV_Target
			{
				float minBary = min(i.bary.x, min(i.bary.y, i.bary.z));
				float b = 1 - step(1-minBary, 0.95);
				// sample the texture
				fixed4 col = tex2D(_MainTex, i.uv) + (_WireframeColor * b * 2 * (1-i.bary.w));
				return col;
			}
			ENDCG
		}
	}
}
