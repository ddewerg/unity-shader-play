﻿Shader "Unlit/UnlitFragmenter"
{
	Properties
	{
		_MainTex ("Texture", 2D) = "white" {}
		_DeformAmount("Deform", Float) = 0.5
		_DeformDistance("EffectorDistance", Float) = 0.5
		_DeformOffset ("Offset", Float) = 0
	}
	SubShader
	{
		Tags { "RenderType"="Opaque" }
		LOD 100


		Pass
		{
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			#pragma geometry geom
			
			#include "UnityCG.cginc"
			#include "Common.cginc"

			struct appdata
			{
				float4 vertex : POSITION;
				float3 normal : NORMAL;
				float2 uv : TEXCOORD0;
				float distanceFromCentre : TEXCOORD1;
			};

			struct varyings
			{
				float4 pos : SV_POSITION;
				float3 normal : NORMAL;
				float2 uv : TEXCOORD0;
			};

			sampler2D _MainTex;
			float4 _MainTex_ST;
			

			void vert (inout appdata v)
			{
				v.distanceFromCentre = length(v.vertex);
				v.vertex = mul(unity_ObjectToWorld, v.vertex);
				v.normal = UnityObjectToWorldNormal(v.normal);
				
			}
			

			varyings VOut(float3 wpos, half3 wnrm, float2 uv)
			{
				varyings o;
				o.pos = UnityWorldToClipPos(float4(wpos, 1));
				o.normal = wnrm;
				o.uv = uv;
				return o;
			}

			float _DeformAmount;
			float _DeformOffset;
			float _DeformDistance;
			float4 DeformPoints;

			[maxvertexcount(12)]
			void geom(triangle appdata input[3], uint pid : SV_PrimitiveID, inout TriangleStream<varyings> outStream)
			{
				uint seed = pid * 877 + uint(_Time.z);
				uint seed2 = pid * 877 + uint(_Time.z + 1);
				float dSec = _Time.z - floor(_Time.z);
				
				float3 n_0 = input[0].normal;
				float3 n_1 = input[1].normal;
				float3 n_2 = input[2].normal;

				float d0 = 0;
				float d1 = 0;
				float d2 = 0;
				for (int i = 0; i < 1023; i++)
				{
					d0 += ((_DeformDistance - (min(_DeformDistance, length(DeformPoint[i].xyz - input[0].vertex.xyz)))) / _DeformDistance) * DeformPoint[i].w;
					d1 += ((_DeformDistance - (min(_DeformDistance, length(DeformPoint[i].xyz - input[1].vertex.xyz)))) / _DeformDistance) * DeformPoint[i].w;
					d2 += ((_DeformDistance - (min(_DeformDistance, length(DeformPoint[i].xyz - input[2].vertex.xyz)))) / _DeformDistance) * DeformPoint[i].w;
				}

				float3 vOff0 = n_0 * (lerp(Random(seed++), Random(seed2++), dSec) * input[0].distanceFromCentre * _DeformAmount + _DeformOffset) * d0;
				float3 vOff1 = n_1 * (lerp(Random(seed++), Random(seed2++), dSec) * input[1].distanceFromCentre * _DeformAmount + _DeformOffset) * d1;
				float3 vOff2 = n_2 * (lerp(Random(seed++),Random(seed2++), dSec) * input[2].distanceFromCentre * _DeformAmount + _DeformOffset) * d2;

				float3 p_0 = input[0].vertex + vOff0;
				float3 p_1 = input[1].vertex + vOff1;
				float3 p_2 = input[2].vertex + vOff2;

				float d3 = (d0 + d1 + d2) / 3;
				float3 n_3 = cross(normalize(p_0 - p_1), normalize(p_0 - p_2));
				float nOffScale = (length(p_0 - p_1) + length(p_0 - p_2) + length(p_1 - p_2)) / 3;
				float vOff3 = n_3 * lerp(Random(seed++), Random(seed2++), dSec);
				float3 p_3 = ((p_0 + p_1 + p_2) / 3) + n_3 * nOffScale * _DeformAmount;

				float uv_3 = (input[0].uv + input[1].uv + input[2].uv) / 3;

				

				if (_DeformAmount * d3 > 0.01)
				{
					outStream.Append(VOut(p_2, -n_2, input[0].uv));
					outStream.Append(VOut(p_1, -n_1, input[1].uv));
					outStream.Append(VOut(p_0, -n_0, input[2].uv));
					outStream.RestartStrip();

					outStream.Append(VOut(p_2, float3(0, 0, 0), input[2].uv));
					outStream.Append(VOut(p_3, float3(0, 0, 0), input[0].uv));
					outStream.Append(VOut(p_1, float3(0, 0, 0), input[1].uv));
					outStream.RestartStrip();

					outStream.Append(VOut(p_1, float3(0, 0, 0), input[1].uv));
					outStream.Append(VOut(p_3, float3(0, 0, 0), input[2].uv));
					outStream.Append(VOut(p_0, float3(0, 0, 0), input[0].uv));
					outStream.RestartStrip();

					outStream.Append(VOut(p_2, float3(0, 0, 0), input[2].uv));
					outStream.Append(VOut(p_0, float3(0, 0, 0), input[0].uv));
					outStream.Append(VOut(p_3, float3(0, 0, 0), input[1].uv));
					outStream.RestartStrip();
				}
				else
				{
					outStream.Append(VOut(p_0, -n_2, input[0].uv));
					outStream.Append(VOut(p_1, -n_1, input[1].uv));
					outStream.Append(VOut(p_2, -n_0, input[2].uv));
					outStream.RestartStrip();
				}
			}

			fixed4 frag (varyings i) : SV_Target
			{
				// sample the texture
				fixed4 col = tex2D(_MainTex, i.uv);
				return col;
			}
			ENDCG
		}
	}
}
