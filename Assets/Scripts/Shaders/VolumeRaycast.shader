﻿// Upgrade NOTE: replaced '_World2Object' with 'unity_WorldToObject'

// Upgrade NOTE: replaced '_Object2World' with 'unity_ObjectToWorld'

Shader "Unlit/VolumeRaycast"
{
	Properties
	{
		_Steps("Resolution", Range(10, 300)) = 100
		[HideInInspector] _MainTex("Base (RGB) Trans (A)", 3D) = "" {}
		[HideInInspector] _TexDim("TexDims", Int) = 16
	}
	SubShader
	{
		Tags{ "Queue" = "Transparent" "IgnoreProjector" = "True" "RenderType" = "Transparent" }
		LOD 100

		ZWrite Off
		Blend SrcAlpha OneMinusSrcAlpha

		Pass
		{
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			// make fog work
			#pragma multi_compile_fog
			#pragma target 4.0
			
			#include "UnityCG.cginc"

			struct appdata
			{
				float4 vertex : POSITION;
				float2 uv : TEXCOORD0;
			};

			struct v2f
			{
				UNITY_FOG_COORDS(1)
				float4 vertex : SV_POSITION;
				float3 worldPos: COLOR0;
				float3 modelPos: COLOR1;
			};

			int _Steps;

			sampler3D _MainTex;
			
			int _TexDim;

			v2f vert (appdata v)
			{
				v2f o;
				o.vertex = UnityObjectToClipPos(v.vertex);
				o.worldPos = mul(unity_ObjectToWorld, v.vertex).xyz;
				o.modelPos = v.vertex.xyz
				UNITY_TRANSFER_FOG(o,o.vertex);
				return o;
			}
			

			struct Ray {
				float3 Origin;
				float3 Dir;
			};

			struct AABB {
				float3 Min;
				float3 Max;
			};

			bool IntersectBox(Ray r, AABB aabb, out float t0, out float t1)
			{
				float3 invR = 1.0 / r.Dir;
				float3 tbot = invR * (aabb.Min - r.Origin);
				float3 ttop = invR * (aabb.Max - r.Origin);
				float3 tmin = min(ttop, tbot);
				float3 tmax = max(ttop, tbot);
				float2 t = max(tmin.xx, tmin.yz);
				t0 = max(t.x, t.y);
				t = min(tmax.xx, tmax.yz);
				t1 = min(t.x, t.y);
				return t0 <= t1;
			}


			fixed4 frag (v2f i) : SV_Target
			{
				float3 worldViewDir = normalize(i.worldPos - _WorldSpaceCameraPos);
				float3 modelViewDir = normalize(mul(unity_WorldToObject, float4(worldViewDir, 0)).xyz);

				Ray r;
				r.Origin = i.modelPos;
				r.Dir = modelViewDir;
				AABB a;
				a.Min = float3(-1, -1, -1);
				a.Max = float3(1, 1, 1);
				float t0, t1;

				IntersectBox(r, a, t0, t1);


				float3 rayStart = (i.modelPos) + 0.5;
				float3 rayEnd =	((i.modelPos + modelViewDir * t1)) + 0.5;
				float3 step = (rayEnd - rayStart) / _Steps;


				fixed4 col = fixed4(0,0,0,0);

				for (int i = 0; i < _Steps; i++)
				{
					if (col.a <= 1)
					{
						fixed4 sampleCol = tex3D(_MainTex, rayStart + step * i);
						col.a += (sampleCol.a * _TexDim)/_Steps;
						col.xyz = lerp(col.xyz, sampleCol.xyz, (sampleCol.a * _TexDim) / _Steps);
					}
				}
				
				// apply fog
				//UNITY_APPLY_FOG(i.fogCoord, col);
				return col;
			}
			ENDCG
		}
	}
}
