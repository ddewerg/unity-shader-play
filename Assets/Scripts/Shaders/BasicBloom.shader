﻿Shader "Hidden/BasicBloom"
{
	Properties
	{
		_MainTex ("Texture", 2D) = "white" {}
	}
	SubShader
	{
		// No culling or depth
		Cull Off ZWrite Off ZTest Always

		Pass
		{
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			
			#include "UnityCG.cginc"

			struct appdata
			{
				float4 vertex : POSITION;
				float2 uv : TEXCOORD0;
			};

			struct v2f
			{
				float2 uv : TEXCOORD0;
				float4 vertex : SV_POSITION;
			};

			v2f vert (appdata v)
			{
				v2f o;
				o.vertex = UnityObjectToClipPos(v.vertex);
				o.uv = v.uv;
				return o;
			}
			
			sampler2D _MainTex;

			fixed4 frag (v2f i) : SV_Target
			{
				fixed4 color = tex2D(_MainTex, i.uv);
				float2 texel = float2(0,0);
				for (int dx = -9; dx < 9; dx++)
				{
					for (int dy = -9; dy < 9; dy++)
					{
						fixed4 col = tex2D(_MainTex, i.uv + float2(dx*texel.x, dy*texel.y));
						color += 1.0 / (1.0 + 0.1*(pow(dx, 4) + pow(dy, 4))) * fixed4(pow(col, fixed4(8, 8, 8, 8)));
					}
				}
				return color;
			}
			ENDCG
		}
	}
}
